function AmbiguousMotion
    % This code generates 3 motion patterns, each consisting of two snapshots
    % of an image at successive times.  It then computes the likelihood of
    % all motions with x-velocities in {-2, -1, 0, 1, 2} and y-velocities in
    % {-2, -1, 0, 1, 2}.
    clear all;
    close all;

    %prior = compute_uniform_prior();
    prior = compute_down_prior();
    rx = 0.75;
    ry = -0.6;
    bx = 1.4;
    by = -0.2;
    posterior = compute_posterior(rx, ry, bx, by, prior);


    
    %{
    Compute likelihood for a measurement, given a gaussian mean and std dev
    %}
    function likelihood = compute_likelihood(observation, mean, sigma)
        likelihood = (1 / sqrt(2*pi*sigma^2) ) * exp(-(observation - mean)^2 / (2*(sigma^2) ));

    % Uniform prior
    function prior = compute_uniform_prior()
        prior = [ 0.25 0.25 0.25 0.25 ]   
        plot_prior(prior, 'Prior (uniform)');

    % Allocate a prior that is 5x more likely to be down 
    function prior = compute_down_prior()
        prior = [ 0.125 0.625 0.125 0.125 ]
        plot_prior(prior, 'Prior (down bias)');

    %{
        Compute the posterior for each direction
    %}
    function posterior = compute_posterior(rx, ry, bx, by, prior)
        noise_sigma = 5;
        up = compute_likelihood(rx, 0, noise_sigma) * compute_likelihood(ry, 1, noise_sigma) * ...
            compute_likelihood(bx, 0, noise_sigma) * compute_likelihood(by, 1, noise_sigma)
        down = compute_likelihood(rx, 0, noise_sigma) * compute_likelihood(ry, -1, noise_sigma) * ...
            compute_likelihood(bx, 0, noise_sigma) * compute_likelihood(by, -1, noise_sigma)
        left = compute_likelihood(rx, -1, noise_sigma) * compute_likelihood(ry, 0, noise_sigma) * ...
            compute_likelihood(bx, -1, noise_sigma) * compute_likelihood(by, 0, noise_sigma)
        right = compute_likelihood(rx, 1, noise_sigma) * compute_likelihood(ry, 0, noise_sigma) * ...
            compute_likelihood(bx, 1, noise_sigma) * compute_likelihood(by, 0, noise_sigma)

        posterior = [up down left right] .* prior
        plot_posterior(posterior, num2str(noise_sigma));

    function plot_posterior(posterior, szSigma)
        imageData = zeros(5,5);
        %set column 1 row 2 - 4 to the left value
        imageData(2:4,1) = posterior(3);
        %set column 5 row 2 - 4 to the right value
        imageData(2:4,5) = posterior(4);
        %set row 1 column 2 - 4 to the up value
        imageData(1,2:4) = posterior(1);
        %set row 5 column 2 - 4 to the down value
        imageData(5,2:4) = posterior(2);

        figure(3)


        colormap(hot)
        imagesc(imageData)
        axis square
        axis on
        set(gca,'xticklabel', {})
        set(gca,'xtick', {})
        set(gca,'yticklabel', {})
        set(gca,'ytick', {})
        colorbar
        szTitle = ['Posteriors R(0.75,-0.6), B(1.4,-0.2), \sigma= ', szSigma];
        title(szTitle);

    function plot_prior(prior, szTitle)
        imageData = zeros(5,5);
        %set column 1 row 2 - 4 to the left value
        imageData(2:4,1) = prior(3);
        %set column 5 row 2 - 4 to the right value
        imageData(2:4,5) = prior(4);
        %set row 1 column 2 - 4 to the up value
        imageData(1,2:4) = prior(1);
        %set row 5 column 2 - 4 to the down value
        imageData(5,2:4) = prior(2);

        figure(4)

        colormap(hot)
        imagesc(imageData)
        axis square
        axis on
        set(gca,'xticklabel', {})
        set(gca,'xtick', {})
        set(gca,'yticklabel', {})
        set(gca,'ytick', {})
        colorbar
        %szTitle = ['Priors'];
        title(szTitle);