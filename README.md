#CSCI7222 Assignment 3 - Geoffrey Sutcliffe
---
###*motion_perception - https://github.com/geoffsutcliffe/motion_perception*
---
##Part I

![](img/part1.jpg)

	function plotprob
	close all
	clear all
	hold off
	models = 0:.1:1;
	priors = ones(size(models))/size(models,2)
	h = 0; t = 0;
	ct = 1;
	label = 'priors';
	for outcome = [1 0 0 1 0 0 0 1 ]
	   subplot(2,4,ct)
	   ct
	   posterior = generate_posteriors (h, t, priors, models);
	   hold off
	   plot(models, posterior,'*');
	   hold on
	   % Plot the beta function parameterized on the count of heads+1 and count of tails+1
	   % This should match the form of the plots.
	   plot(models,betapdf(models, h+1, t+1)) 
	   title(label);
	   xlabel('model');
	   set(gca,'ylim',[0 3]);
	   h = h + outcome;
	   t = t + (1-outcome);
	   label = 'T';
	   if (outcome)
	      label = 'H';
	   end
	   label = sprintf('trial %d: %s',ct,label);
	   
	   ct = ct + 1;
	end
	%print -depsc2 simulate_coin_flip_discrete.eps
	
	function posterior = generate_posteriors(h,t,prior,p)
	% compute posterior of hypothesis "p" given "h" heads, "t" tails and prior for p
	
	posterior = prior .* p.^h .* (1-p).^t;
	posterior = posterior ./ sum(posterior);



##Part II
Below are the code snippets that produce the priors and posterior probabilities 

    function prior = compute_prior(velocityRange, sigma)
        numVelocities = length(velocityRange);
        prior = zeros(numVelocities, numVelocities);

        for columnVelocityIndex = 1:numVelocities
           for rowVelocityIndex = 1:numVelocities
               columnVelocity = velocityRange(columnVelocityIndex);
               rowVelocity = velocityRange(rowVelocityIndex);
               %Originally had an error in here - I was plugging in the col/row velocity index, rather than the value
               prior(columnVelocityIndex,rowVelocityIndex) = (1 / (2 * sigma^2)) * (columnVelocity^2 + rowVelocity^2);
           end
        end



		function posterior = compute_posterior(image1, image2, velocityRange, sigma)
		ll = compute_loglikelihood(image1, image2, velocityRange);
		prior = compute_prior(velocityRange, sigma);
		plot_prior(prior, 1, velocityRange, sigma);
		posterior = ll-prior;


Below are the image of the prior and the posterior probabilities with 'extreme' values of &sigma;, with &sigma; = 20.

![](img/part2_prior20.jpg) 
![](img/part2_posterior20.jpg)
---

Compare them with &sigma; = 1. 

![](img/part2_prior1.jpg)
![](img/part2_posterior1.jpg)

What this shows is that with a strong prior (&sigma;=1), and not much evidence, the posterior distribution is almost exactly equal to the prior - it does not place much faith in the evidence. With a weak prior (&sigma;=20), the posterior probability is almost equal to the likelihood - the evidence is believed.

---
A more interesting result is seen with &sigma;=3.

![](img/part2_posterior3.jpg)

What we see here is a slight bias towards the prior (slow movements). In the first example, the Maximum A-Posteriori (MAP) is favouring the result {(1,0)} - because in the likelihood value, the other two values considered have a y-velocity-component of 2 - {(2,2), (0,-2)}. In the second example we see greater *confusion* than in the likelihoods -- there is less confidence about the velocity being {(0.-1) or (1,1)}. The effect of the addition of the prior in this case is perhaps reflecting the *perceived* velocity (uncertainty about where the object is moving). In the third case, we again see a bias towards low velocity. The likelihoods produce a velocity estimate of {(2,1)}, but with the addition of the priors, because we are biased towards low velocity, the predicted velocity is {(0,1)}.


##Part III
First, a visualization of the priors used in this exercise.

####(up,down,left,right):[0.25, 0.25, 0.25, 0.25]

![](img/part3_prior_uniform.jpg)

####(up,down,left,right):[0.125, 0.625, 0.125, 0.125]

![](img/part3_prior_downbias.jpg)


##Task 1
![](img/part3_posterior1_uniform.jpg)

##Task 2
![](img/part3_posterior5_uniform.jpg)

The difference between Task 1 and Task 2 is the increase of the sigma value for the assumed noise added to the observations. Although the MAP is the same in both cases, the confidence of the result where noise is included, is much lower. We still come to the reasonable conclusion of movement to the right, but we admit the possibility that movement is in one of the other directions.

##Task 3
![](img/part3_posterior1_downbias.jpg)

The difference between Task 1 and Task 3 is the use of the 'down bias' prior. The effect is that, even though the data is indicating that the object is moving to the right, the confidence (from limited data) is not strong enough to overrule the prior that favours moving downwards.

##Task 4
![](img/part3_posterior5_downbias.jpg)

The difference in Task 4 is that we are admitting a large degree of noise in the observations, and using the 'down bias' prior. The first effect is that the MAP is down (same as in the previous Task). The other, more interesting effect is that we place almost no confidence in the observations - the right motion is almost as unlikely as the up and left motions.

##Code for Part III
    %{
    Compute likelihood for a measurement, given a gaussian mean and std dev
    %}
    function likelihood = compute_likelihood(observation, mean, sigma)
        likelihood = (1 / sqrt(2*pi*sigma^2) ) * exp(-(observation - mean)^2 / (2*(sigma^2) ));

    %{
        Compute the posterior for each direction
    %}
    function posterior = compute_posterior(rx, ry, bx, by, prior)
        noise_sigma = 1;
        up = compute_likelihood(rx, 0, noise_sigma) * compute_likelihood(ry, 1, noise_sigma) * ...
            compute_likelihood(bx, 0, noise_sigma) * compute_likelihood(by, 1, noise_sigma)
        down = compute_likelihood(rx, 0, noise_sigma) * compute_likelihood(ry, -1, noise_sigma) * ...
            compute_likelihood(bx, 0, noise_sigma) * compute_likelihood(by, -1, noise_sigma)
        left = compute_likelihood(rx, -1, noise_sigma) * compute_likelihood(ry, 0, noise_sigma) * ...
            compute_likelihood(bx, -1, noise_sigma) * compute_likelihood(by, 0, noise_sigma)
        right = compute_likelihood(rx, 1, noise_sigma) * compute_likelihood(ry, 0, noise_sigma) * ...
            compute_likelihood(bx, 1, noise_sigma) * compute_likelihood(by, 0, noise_sigma)

        posterior = [up down left right] .* prior
        plot_posterior(posterior, num2str(noise_sigma));

##Appendix - Results from Part III

<table>
  <tr>
	<td></td>
    <td>Up</td>
    <td>Down</td>
    <td>Left</td>
	<td>Right</td>
  </tr>
  <tr>
	<td>Task 1</td>
    <td>0.0002</td>
    <td>0.0012</td>
    <td>0.0001</td>
	<td>0.0046</td>
  </tr>
  <tr>
	<td>Task 2</td>	
    <td>1.0e-04*0.0889</td>
    <td>1.0e-04*0.0948</td>
    <td>1.0e-04*0.0843</td>
	<td>1.0e-04*0.1001</td>
  </tr>
  <tr>
	<td>Task 3</td>
    <td>0.0001</td>
    <td>0.0030</td>
    <td>0.0000</td>
	<td>0.0023</td>
  </tr>
  <tr>
	<td>Task 4</td>
    <td>1.0e-04*0.0445</td>
    <td>1.0e-04*0.2370</td>
    <td>1.0e-04*0.0421</td>
	<td>1.0e-04*0.0500</td>
  </tr>
</table> 